// Aggregation in MongoDB
	- match and group set of fields
	- are needed when your application needs a form of information that is NOT VISIBLY available from your documents



// Aggregation Pipeline
	- consist of stages

_______________________________________________
TO RETRIEVE AND FILTER THE DATA TO BE RETRIEVED
		db.fruits.aggregate([
			// to match
			{ $match: { onSale: true } }, 

			// group, and filter (WE PUT $ TO SPECIFY THAT THE DATA IS AVAILABLE IN THE FIELD)
			{ $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },

			// to display or not
			{ $project: { _id: 0 } }
		])

			OUTPUT: (1) total = 45.0
					(2) total = 15.0

__________________
THE PROJECT METHOD
		db.fruits.aggregate([
	
		    // if "NULL", means to just count the available values, then save it to the next statement
		    { $group: { _id: null, myCount: { $sum: 1 } } },		

		    { $project: { _id: 0 } }
		])

			OUTPUT: myCount = 4.0


____________________________
TO SORT DATA TO BE RETRIEVED
		(-1) reverse order
		( 1) for normal order


		db.fruits.aggregate([
					// to match
					{ $match: { onSale: true } }, 

					// group, and filter
					{ $group: { _id: "$supplier_id", total: { $sum: "$stock" } } },
		                        
                    // to sort 
                    { $sort: { total: -1 } }
				])


________________________
TO DECONSTRUCT AND GROUP
		$unwind - deconstruct a specific field ina document
				- transforms complex documents into simpler documents

		db.fruits.aggregate([
			{ $unwind: "$origin" },
            { $group: { _id: "$origin", kinds: { $sum: 1 } } }
		])

			OUTPUT: Displaying how many fruits every origin has


________________
TO COUNT A QUERY
			db.scores.aggregate([
				{ $match: { score: { $gte: 80 } } },

				// any string can be used as long as no $ sign is used, to create a new field
			    { $count: "passing_score" }				
			])


_________________
FROM BOODLE NOTES



		// [Section] MongoDB Aggregation
		/*
			- Used to generate manipulated data and perform operations to create filtered results that helps in analyzing data
			- Compared to doing CRUD operations on our data from previous sessions, aggregation gives us access to manipulate, filter and compute for results providing us with information to make necessary development decisions without having to create a frontend application.
		*/

		// Using the aggregate method
		
			/*- The "$match" is used to pass the documents that meet the specified condition(s) to the next pipeline stage/aggretation process.
			- Syntax
				- { $match: { field: value } }*/


			/*- The "$group" is used to group elements together and field-value pairs using the data from the grouped elements
			- Syntax
				- { $group: { _id: "value", fieldResult: "valueResult" } }*/
          /*_id => Required. If you specify an _id value of null, or any other constant value, the $group stage calculates accumulated values for all the input documents as a whole. See example of Group by Null.
          field => Optional. Computed using the accumulator operators.*/

			/*- Using both "$match" and "$group" along with aggregation will find for products that are on sale and will group the total amount of stocks for all suppliers found.
			- Syntax
				- db.collectionName.aggregate([
					{ $match: { fieldA, valueA } },
					{ $group: { _id: "$fieldB" }, { result: { operation } } }
				])
			- The "$" symbol will refer to a field name that is available in the documents that are being aggregated on.
			- The "$sum" operator will total the values of all specified fields in the returned documents that are found using the "$match" criteria.*/

		// Field projection with aggregation
		/*
			- The "$project" can be used when aggregating data to include/exclude fields from the returned results
			- Syntax
				- { $project : { field: 1/0 } }
		*/

		// Sorting aggregated results
		/*
			- The "$sort" can be used to change the order of aggregated results
			- Providing a value of -1 will sort the aggregated results in a reverse order
			- Syntax
				- { $sort { field: 1/-1 } }
		*/
 	
		// Aggregating results based on array fields
		/*
			- The "$unwind" deconstructs an array field from a collection/field with an array value to output a result for each element.
			- The syntax below will return results creating separate documents for each of the countries provided per the "origin" field
			- Syntax
				- { $unwind: field }
		*/
//$sum : 1 => add 1 
		// Displays fruit documents by their origin and the kinds of fruits that are supplied
		db.fruits.aggregate([
			{ $unwind : "$origin" },
			{ $group : { _id : "$origin" , kinds : { $sum : 1 } } }
		]);

		//per origin
		//+1 dami ng bansa, pag 2 ma d doble yung number. 

//Display the Color documents
		db.fruits.aggregate([
			{ $unwind : "$color" },
			{ $group : { _id : "$color" , kinds : { $sum : 1 } } }
		]);


/*

yung kinds, user defined yan... na based sa origin ng fruits... i-aadd nya yung total number of kinds of fruits na meron per country.
so like yung aggregation syntax na yan... ang ipiprint out nya is for every country... ilang kinds of fruits yung makikita niya sa buong collection.
kinds meaning... ilang fruits meron sinusupply yung philippines... banana... mango... etc.


Ecuador - banana
India - Mango
China - kiwi
Philippines - apple, banana, Mango
US - apple, Kiwi



*/
//$count

//Insert
db.scores.insertMany([
	{ "_id" : 1, "subject" : "History", "score" : 88 },
	{ "_id" : 2, "subject" : "History", "score" : 92 },
	{ "_id" : 3, "subject" : "History", "score" : 97 },
	{ "_id" : 4, "subject" : "History", "score" : 71 },
	{ "_id" : 5, "subject" : "History", "score" : 79 },
	{ "_id" : 6, "subject" : "History", "score" : 83 }
])


/*

The following aggregation operation has two stages:

1. The $match stage excludes documents that have a score value of less than or equal to 80 to pass along the documents with score greater than 80 to the next stage.
2. The $count stage returns a count of the remaining documents in the aggregation pipeline and assigns the value to a field called passing_scores.
*/

db.scores.aggregate(
[
    {$match: {
        score: {
            $gt: 80
            }
        }
    },
    {
        $count: "passing_scores"
     }
]

)


/*{ $count: <string> }
<string> is the name of the output field which has the count as its value. <string> must be a non-empty string, must not start with $ and must not contain the . character.
*/