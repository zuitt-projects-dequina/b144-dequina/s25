db.fruits.aggregate([

    { $match: { stock: { $gte: 20 } } },
    
    { $count: "fruits_with_20up_stocks" }
])