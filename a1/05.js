db.fruits.aggregate([
	
	{ $match: { onSale: true } }, 

	{ $group: { _id: "$supplier_id", average_price: { $avg: "$price" } } },

	{ $project: { _id: 0 } },
    
    { $sort: { total: -1 } }
])