db.fruits.aggregate([

    { $match: { onSale: true } },
    
    { $count: "fruits_on_sale" }
])